#! /usr/bin/python
# -*- coding: utf-8 -*-

""

import csv
import pyExcelerator as xlwt
import xlrd
from time import time


def write_xls(data,colnames,filename):
    wb = xlwt.Workbook()
    sht = wb.add_sheet("sheet1")
    for i,n in enumerate(colnames):
        sht.write(0,i,n)
    for r in range(0,len(data)):
        for c in range(len(colnames)):
            try:
                sht.write(r+1, c, data[r][c])
            except Exception, e:
                print e
                pass
    wb.save(filename)


def write_csv(data,colnames,filename):
    f = open(filename, 'wb')
    writer = csv.writer(f, colnames,
        quoting=csv.QUOTE_ALL)
    writer.writerow(colnames)
    for r in data:
        writer.writerow([d.encode('utf8') 
            if type(d)==unicode else d 
            for d in r])
    f.close()


def read_xls(filename,header=False):
    wb = xlrd.open_workbook(filename)
    sht = wb.sheet_by_index(0)
    data = []
    ncols = sht._dimncols
    for r in range(header,sht._dimnrows):
        v = sht.row_values(r)
        if ncols == 3:
            data.append([int('%010i' % v[0]),v[1],v[2]])
        elif ncols == 2:
            data.append([hash(time()),v[0], v[1]])
        elif ncols == 1:
            data.append([hash(time()),v[0],'unk'])
    return data


def read_csv(filename,header=False):
    data = []
    lidReader = csv.reader(open(filename, 'rb'),
            delimiter=',', quotechar='"')
    if header: lidReader.next()
    row = lidReader.next()
    while row:
        ncols = len(row)
        if ncols == 3:
            data.append([int('%010i' % row[0]),row[1].decode('utf8'),row[2]])
        elif ncols == 2:
            data.append([hash(time()),row[0].decode('utf8'), row[1]])
        elif ncols == 1:
            data.append([hash(time()),row[0].decode('utf8'),'unk'])
        try:
            row = lidReader.next()
        except:
            row = None
    return data


def read_txt(filename,lang=None,header=False):
    f = open(filename, 'rb')
    rows = f.readlines()
    f.close()
    if header: rows = rows[1:]
    if lang:
        data = [[hash(time()),row.decode('utf8'), lang] 
                for row in rows]
    else:
        data = [[hash(time()),row.decode('utf8'), 'unk'] 
                for row in rows]
    return data



READERS = {'csv':read_csv,'xls':read_xls,'txt':read_txt}
WRITERS = {'csv':write_csv,'xls':write_xls}


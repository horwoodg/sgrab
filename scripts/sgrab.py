#!/usr/bin/python
# encoding: utf-8

"""
@author: graham

TODO: Find browser that can handle js.
TODO: Topic classifier filter.
TODO: Keyword filter.
TODO: HTTP header and crawl time randomization.
TODO: Better sentence tokenization.
TODO: Config file

"""

import sys, os

ROOT = os.path.dirname(os.path.realpath(__file__))
sys.path.append(ROOT+'/../lib/python/')

import re
import mechanize, cookielib
from time import asctime
from urlparse import urlparse
from optparse import OptionParser
from signal import signal, alarm, SIGALRM
from readability.readability import Document
from chardet import detect 
from lid import *



br = mechanize.Browser()
cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)
br.set_handle_equiv(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(True)
br.set_handle_refresh(True, honor_time=True, max_time=10)
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
br.set_debug_http(False)
br.set_debug_redirects(False)
br.set_debug_responses(False)



def blacklist():
    with open(ROOT+"/../data/blacklist.txt",'r') as fi:
        lines = [line.strip().decode('utf8').lower()
                for line in fi.readlines()
                if line and line[0]!="#"]
    return re.compile(".*\.("+'|'.join(lines)+")$",re.I)

BLACKLIST = blacklist()

ENDPUNC = u'[.?!\u0964\u0965\u10fb\u203b-\u203d\u2047-\u2049\u1367\u1368\u061f]'

RE = {
      'exclude':(re.compile(u'.*(error|links|contact|about|gallery|javascript|login|reset|search|mailto|images*|photos*|\/$|http.*http).*',re.I|re.S),""),
      'domain':(re.compile(u'(https*:\/\/)*(www\.*)*(.*)',re.I|re.S),"\\3"),
      'comment':(re.compile(u'<!--.*?-->',re.M|re.S)," "),
      'punct':(re.compile(u'[,.\\/?><\';\":\]\[|}{`~!@#$%^&*()_+=-]')," "),
      'html':(re.compile(u'<.*?>|&lt;.*?&gt;|&.*?;',re.M|re.S)," "),
      'S':(re.compile(u'\s*(?<! .)(?<!\.)('+ENDPUNC+u')  *',re.M|re.S),"\\1\n"),
      'dedupS':(re.compile(u'(^\w*?\.$\n*)(?=.*\\1.*)',re.M|re.S)," "),
      'lists':(re.compile(u'^.*([:|\u2022\u2191·]).*?\\1.*?$\n*',re.M)," "),
      'white':(re.compile(u'^  *|  *$|[ \t](?=\s|$)',re.M|re.S),""),
      }


def sanitize(text, ptns=None):
    """
    Unless otherwise specified, this function:
    1) strips html
    2) inserts linebreaks at sentence boundaries (.?!)
    3) removes duplicate lines
    4) strips lines with 2+ of '|', '•', '·', '↑', ':'
    5) strips extra whitespace
    6) removes lines with fewer than 40 characters
    7) removes lines which don't end in sentence punctuation (.?!)
    """
    if not ptns: ptns = ['html','S','dedupS','lists','white']
    for ptn in ptns:
        text = RE[ptn][0].sub(RE[ptn][1],text)
    text = '\n'.join([l.strip() for l in text.split('\n') 
        if re.match(u'.*'+ENDPUNC+u'\s*$',l) and len(l)>=30]).strip()
    return text


class bcolors:
    """For color coded ANSI output."""
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''


class TimeoutFunctionException(Exception): 
    """Exception to raise on a timeout""" 
    pass 


class TimeoutFunction:
    def __init__(self, function, timeout): 
        self.timeout = timeout 
        self.function = function 
    def handle_timeout(self, signum, frame): 
        raise TimeoutFunctionException()
    def __call__(self, *args): 
        old = signal(SIGALRM, self.handle_timeout) 
        alarm(self.timeout) 
        try: 
            result = self.function(*args)
        finally: 
            signal(SIGALRM, old)
        alarm(0)
        return result 


class SGrab(object):
    """
    Crawls target sites, extracts sentences
    and writes out to text files.
    """
    def __init__(self,limit=None,timeout=5,
            minwd=5,maxwd=20000,ppc=0.5,wpt=2,wps=100,
            outfile=None, url=None, verbose=False,
            lang='', lid_model='all', lid_threshold=0.3,
            encoding=None):
        self.outfile = outfile
        self.pages = {}
        self.links = {}
        self.visited = {}
        if url:
            set_url(url)
        if outfile and os.path.isfile(outfile):
            self.read_summary_hashes()
            print "Collected %d crawled page hashes." % len(self.summaries)
        else:
            self.summaries = {}
        self.limit = limit
        self.timeout = timeout
        self.minwd = minwd
        self.maxwd = maxwd
        self.ppc = ppc
        self.wpt = wpt
        self.wps = wps
        self.verbose = verbose
        self.lang = lang
        self.lid_model = lid_model
        self.lid_threshold = lid_threshold
        self.encoding = encoding
    def set_url(self,url):
        """
        Set root url and domain.
        """
        self.root_url = url
        self.domain = urlparse(url).netloc
    def get_summaries(self):
        """
        Works through a dictionary of links, up to a user-provided
        link limit. Spider updates a master link list with every 
        iteration, only adding links which have not previously been
        crawled.
        """
        linkgen = lambda br: dict((l.absolute_url,0) for l in br.links()       
                    if (not RE['exclude'][0].match(l.absolute_url)) and
                       (not BLACKLIST.match(l.absolute_url)) and
                       (not self.visited.has_key(l.absolute_url)) and
                        re.match(".*"+self.domain+".*",l.absolute_url))
        if not self.links:
            self.links = {self.root_url:0}
        i = 0
        while True:
            if i==self.limit:
                print bcolors.FAIL + "Page limit reached." + bcolors.ENDC
                break
            if len(self.links)==0:
                print bcolors.FAIL + "No more links." + bcolors.ENDC
                break
            else:
                url = self.links.popitem()[0]
                self.visited[url] = 1
            if self.verbose: print "Spidering:", url
            try:
                resp = br.open(url, timeout=self.timeout)
                pages = resp.read()
                encoding = self.encoding or detect(pages)['encoding']
                if self.verbose:
                    print "  detected encoding:", encoding
                self.pages[url] = pages.decode(encoding,'ignore')
                self.read_page(url,self.pages[url])
                self.links.update(linkgen(br))
            except Exception,e:
                if self.verbose: 
                    print bcolors.FAIL + "  Exception: " + str(e) + bcolors.ENDC
                continue
            i += 1
    def read_page(self, url, text):
        """
        Extracts content from page. Times out if page parsing takes
        longer than $timeout number of seconds. Does light text
        analysis of page to determine suitablity for collection, 
        dropping content which is above/below user-input thresholds.
        """
        try:
            doc = Document(text)
            get_summary = TimeoutFunction(doc.summary,self.timeout)
            summary = get_summary()
        except TimeoutFunctionException:
            if self.verbose:
                print bcolors.FAIL + "  Unable to parse." + bcolors.ENDC
            return
        except Exception, e:
            if self.verbose:
                print bcolors.FAIL + e.message + bcolors.ENDC
            return
        tag_freq = len(RE['html'][0].findall(summary))*1.0
        try:
            get_sanitized = TimeoutFunction(sanitize,self.timeout)
            summary = get_sanitized(summary)
            if self.lid_model and len(summary) > 0:
                summary = self.language_filter(summary)
        except TimeoutFunctionException:
            if self.verbose:
                print bcolors.FAIL + "  Unable to sanitize." + bcolors.ENDC
            return
        summary_hash = hash(summary)
        if summary_hash==0:
            if self.verbose:
                print bcolors.FAIL + "  No valid sentences." + bcolors.ENDC
            return
        p_freq = len(RE['punct'][0].findall(summary))*1.0
        chr_freq = len(summary)*1.0
        word_freq = len(re.split('(?s)\s',summary))*1.0
        sent_freq = len(summary.split(u'\n'))*1.0
        if not self.summaries.get(summary_hash,0):
            if self.verbose:
                print "  wc:%d, ppc:%.2f, wpt:%.2f, wps:%.2f" % (word_freq,
                    p_freq/chr_freq, word_freq/tag_freq, word_freq/sent_freq)
            if (self.minwd <= word_freq <= self.maxwd and 
                    round(p_freq/chr_freq,2) <= self.ppc and
                    round(word_freq/tag_freq,2) >= self.wpt and 
                    round(word_freq/sent_freq,2) <= self.wps):
                if self.verbose:
                    print bcolors.OKBLUE + summary.encode('utf-8') + bcolors.ENDC
                if self.outfile:
                    self.write(summary,url)
            elif self.verbose:
                print bcolors.FAIL + summary.encode('utf-8') + bcolors.ENDC
            self.summaries[summary_hash] = 1
        else:
            if self.verbose:
                print bcolors.FAIL + "  Duplicate page." + bcolors.ENDC
    def write(self,summary,url):
        """ Writes data out to a text file, one sentence per line."""
        f = open(self.outfile,'a')
        for i, line in enumerate(summary.split('\n')):
            sid = ','.join((self.domain,str(abs(hash(url))),str(i)))
            out = "%s\t%s\t%s\t%s\t%s\n" % (line.strip(), sid, asctime(), self.domain, url)
            f.write(out.encode('utf-8','ignore'))
        f.close()
    def concept_filter(self,classifier):
        """
        TODO: Statically classify page content as collected, only returning
        content for a particular domain.
        """
        pass
    def keyword_filter(self, word_list):
        """
        TODO: Search pages for user-input keywords, only returning 
        content which matches.
        """
        pass
    def language_filter(self, summary):
        sentences = summary.split(u'\n')
        for i, sentence in enumerate(sentences):
            lang, p = identify(sentence,self.lid_model)
            target = self.lang or 'unk'
            if lang != target and p >= self.lid_threshold:
                sentences[i] = ""
        return '\n'.join([s for s in sentences if s is not '']).strip()
    def read_visited(self,filename):
        with file(filename+".visited",'r') as fi:
            lines = [line.strip().split("\t")
                for line in fi.readlines()]
        self.visited = dict([(line[0], int(line[1])) 
            for line in lines if line[1]=='1'])
        self.links = dict([(line[0], int(line[1])) 
            for line in lines if line[1]=='0'])
    def write_visited(self,filename):
        with file(filename+".visited",'w') as fo:
            for url,v in self.links.iteritems():
                fo.write("%s\t%s\n" % (url,v))
            for url,v in self.visited.iteritems():
                fo.write("%s\t%s\n" % (url,v))
    def read_summary_hashes(self):
        with file(self.outfile,'r') as fi:
            ids = [line.strip().decode('utf8').split('\t')[1].split(',')
                for line in fi.readlines()]
        hashes = set([int(i[1]) for i in ids 
            if len(i)==3 and i[1].isdigit()])
        self.summaries = dict([(h,1) for h in hashes])


if __name__ == '__main__':

    usage = "usage: python %prog [options] <url|urlfile> <outfile>"
    parser = OptionParser(usage)
    parser.add_option("-t", "--timeout", dest="timeout",
            type="int", default=5, help="page open timeout in sec.")
    parser.add_option("-v", "--verbose", action="store_true", 
            dest="verbose", help="display system messages")
    parser.add_option("-R", "--ignore-robots", action="store_true", 
            dest="robots", help="ignore robots.txt")
    parser.add_option("-q", "--querylimit", dest="limit",
            type="int", default=None, help="maximum page queries per site; set '1' for non-recursive crawls")
    parser.add_option("-e", "--encoding", dest="encoding",
            type="string", default=None, help="site encoding")
    parser.add_option("-x", "--maxwd", dest="maxwd",
            type="int", default=10000, help="maximum words/page")
    parser.add_option("-n", "--minwd", dest="minwd",
            type="int", default=5, help="minimum words/page")
    parser.add_option("-p", "--ppc", dest="ppc",
            type="float", default=0.05, 
            help="maximum punctuation markers per character (def=0.05)")
    parser.add_option("-s", "--wps", dest="wps",
            type="float", default=50, 
            help="maximum words per sentence (def=50)")
    parser.add_option("-w", "--wpt", dest="wpt",
            type="float", default=2, help="minimum words per tag (def=2)")
    parser.add_option("-l", "--lang", dest="lang",
            type="string", default=None, 
            help="target language")
    parser.add_option("-m", "--lidmodel", dest="lid_model",
            type="string", default=None, 
            help="language identification model file")
    parser.add_option("-i", "--lidthreshold", dest="lid_threshold",
            type="float", default=0.3, 
            help="min. p(lang|sentence)")
    options, args = parser.parse_args()

    if not args:
        parser.print_help()
        sys.exit(0)

    if options.robots:
        print "Ignoring robots.txt."
        br.set_handle_robots(False)

    if os.path.isfile(args[0]):
        f = open(args[0],"r")
        urls = [l.strip() for l in f.readlines()]
        visitfile = args[0]
        f.close()
    else:
        urls = [args[0]]
        visitfile = "urls"

    if len(args)==2:
        outfile = args[1]
    else:
        outfile = ''

    cr = SGrab(
            limit=options.limit,
            timeout=options.timeout,
            minwd=options.minwd, 
            maxwd=options.maxwd, 
            ppc=options.ppc, 
            wps=options.wps, 
            wpt=options.wpt,
            outfile=outfile,
            verbose=options.verbose,
            lang=options.lang,
            lid_model=options.lid_model,
            lid_threshold=options.lid_threshold,
            encoding=options.encoding)
    
    if os.path.isfile(visitfile+".visited"):
            cr.read_visited(visitfile)
            print "Read in %d previously crawled urls." % len(cr.visited)

    log = open("errors.log",'a')
    log.write("=======SGRAB Crawling Log, %s========\n" % asctime())

    try:
        for url in urls:
            try:
                cr.set_url(url)
                cr.get_summaries()
            except Exception, e:
                log.write(url+" :: "+str(e)+"\n")
                pass
    except SystemExit:
        raise
        cr.write_visited(visitfile)
    except KeyboardInterrupt:
        print "Crawl terminated by user."
        cr.write_visited(visitfile)
    else:
        print "Crawl complete."

    log.close()

#! /usr/bin/python
# -*- coding: utf-8 -*-

"""
    Identify the language of input text.

    Two-pass identifier first determines unicode block, then uses
    a maxent classifier to identify non-unique script languages.

    Unicode block identification adapted from Kent Johnson's 
    python adaptation of guesslanguage.cpp by Jacob R Rideout, 
    an adaptation of Language::Guess by Maciej Ceglowski.
    
"""

import os, re, sys
from collections import defaultdict
from operator import itemgetter
from blocks import unicodeBlock
from itertools import chain
from random import sample
from time import time
from rw import *
from preprocess import *


SINGLETONS = {
    'Armenian': 'hy',
    'Bengali': 'bn',
    'Burmese': 'my',
    'Hebrew': 'he',
    'Georgian': 'ka',
    'Greek': 'el',
    'Greek and Coptic': 'el',
    'Gujarati': 'gu',
    'Gurmukhi': 'pa',
    'Katakana': 'ja',
    'Kannada': 'kn',
    'Khmer': 'km',
    'Korean': 'ko',
    'Lao': 'lo',
    'Oriya': 'or',
    'Malayalam': 'ml',
    'Mongolian': 'mon',
    'Sinhala': 'si',
    'Tamil': 'ta',
    'Telugu': 'te',
    'Thai': 'th',
    'Tibetan': 'bo',
    'Hangul Syllables': 'ko',
    'Hangul Jamo': 'ko',
    'Hangul Compatibility Jamo': 'ko',
    'Hangul': 'ko',
    'Katakana': 'ja',
    'CJK Unified Ideographs': 'zh',
    'Bopomofo' : 'zh',
    'Bopomofo Extended': 'zh',
    'KangXi Radicals': 'zh',
    }


NAME_MAP = {
     'unk': 'Unknown',
     'ab': 'Abkhazian',
     'acm': 'Arabic, Mesopotamian',
     'af': 'Afrikaans',
     'afb': 'Arabic, Gulf',
     'ajp': 'Arabic, North Levantine',
     'apc': 'Arabic, South Levantine',
     'ar': 'Arabic',
     'arb': 'Arabic, MSA',
     'arz': 'Arabic, Egyptian',
     'az': 'Azeri',
     'be': 'Byelorussian',
     'bg': 'Bulgarian',
     'bn': 'Bengali',
     'bo': 'Tibetan',
     'br': 'Breton',
     'ca': 'Catalan',
     'ceb': 'Cebuano',
     'cs': 'Czech',
     'cy': 'Welsh',
     'da': 'Danish',
     'de': 'German',
     'ee': 'Estonian',
     'el': 'Greek',
     'en': 'English',
     'eo': 'Esperanto',
     'es': 'Spanish',
     'et': 'Estonian',
     'eu': 'Basque',
     'fa': 'Farsi',
     'fi': 'Finnish',
     'fil': 'Filipino',
     'fo': 'Faroese',
     'fr': 'French',
     'fy': 'Frisian',
     'gd': 'Scots Gaelic',
     'gl': 'Galician',
     'gu': 'Gujarati',
     'ha': 'Hausa',
     'haw': 'Hawaiian',
     'he': 'Hebrew',
     'hi': 'Hindi',
     'hr': 'Croatian',
     'hu': 'Hungarian',
     'hy': 'Armenian',
     'ind': 'Indonesian',
     'is': 'Icelandic',
     'it': 'Italian',
     'ja': 'Japanese',
     'ka': 'Georgian',
     'kk': 'Kazakh',
     'km': 'Khmer',
     'kn': 'Kannada',
     'ko': 'Korean',
     'ku': 'Kurdish',
     'ky': 'Kyrgyz',
     'la': 'Latin',
     'lo': 'Lao',
     'lt': 'Lithuanian',
     'lv': 'Latvian',
     'mg': 'Malagasy',
     'mk': 'Macedonian',
     'ml': 'Malayalam',
     'mn': 'Mongolian',
     'mon': 'Mongolian',
     'mr': 'Marathi',
     'zsm': 'Malay',
     'my': 'Burmese',
     'nd': 'Ndebele',
     'nep': 'Nepali',
     'nl': 'Dutch',
     'nn': 'Nynorsk',
     'no': 'Norwegian',
     'nso': 'Sepedi',
     'or': 'Oriya',
     'pa': 'Gurmukhi',
     'pl': 'Polish',
     'ps': 'Pashto',
     'pt': 'Portuguese',
     'ro': 'Romanian',
     'ru': 'Russian',
     'sa': 'Sanskrit',
     'sh': 'Serbo-Croatian',
     'si': 'Sinhala',
     'sk': 'Slovak',
     'sl': 'Slovene',
     'so': 'Somali',
     'sq': 'Albanian',
     'sr': 'Serbian',
     'sv': 'Swedish',
     'sw': 'Swahili',
     'ta': 'Tamil',
     'te': 'Telugu',
     'th': 'Thai',
     'tl': 'Tagalog',
     'tn': 'Setswana',
     'tr': 'Turkish',
     'ts': 'Tsonga',
     'tw': 'Twi',
     'uk': 'Ukranian',
     'ur': 'Urdu',
     'uz': 'Uzbek',
     've': 'Venda',
     'vi': 'Vietnamese',
     'xh': 'Xhosa',
     'zh': 'Chinese',
     'zh-tw': 'Traditional Chinese (Taiwan)',
     'zu': 'Zulu'
    }


SCRIPTS = {
    'sr':'latin',
    'sq':'latin',
    'ro':'latin',
    'hr':'latin',
    'tr':'latin',
    'nl':'latin',
    'en':'latin',
    'fi':'latin',
    'da':'latin',
    'no':'latin',
    'ee':'latin',
    'ca':'latin',
    'it':'latin',
    'de':'latin',
    'fr':'latin',
    'pt':'latin',
    'es':'latin',
    'fil':'latin',
    'ind':'latin',
    'zsm':'latin',
    'ar':'arabic',
    'acm':'arabic',
    'ajp':'arabic',
    'arz':'arabic',
    'afb':'arabic',
    'apc':'arabic',
    'fa':'arabic',
    'gbz':'arabic',
    'ps':'arabic',
    'ur':'arabic',
    'mk':'cyrillic',
    'bg':'cyrillic',
    'ru':'cyrillic',
    'hi':'devanagari',
    'nep':'devanagari',
    'unk':'unk'
    }


def flatten(LoL):
    return chain.from_iterable(LoL)


def read_txt(filename,lang=None,header=False):
    f = open(filename, 'rb')
    rows = f.readlines()
    f.close()
    if header: rows = rows[1:]
    if lang:
        data = [[hash(time()),row.decode('utf8'), lang] 
                for row in rows]
    else:
        data = [[hash(time()),row.decode('utf8'), 'unk'] 
                for row in rows]
    return data


class PLAINClassifier:
    def __init__(self, script=None, modelfile='all',
                 root="./", tmpdir='/tmp'):
        self.script = script
        self.root = root
        now = hash(time())
        self.tmpdir = tmpdir+"/lid-%d" % now
        if not os.path.exists(self.tmpdir):
            os.makedirs(self.tmpdir)
        self.fl_name = self.tmpdir+"/%d.fl" % now
        self.rf_name = self.tmpdir+"/%d.rf" % now
        self.configfile = self.tmpdir+'/%d.cfg' % now
        self.params = {
                'VERBOSE':0,
                'STOPLIST':'none',
                'MULTIPLE_CLASSES':0,
                'XML_STYLE_FILELIST':0,
                'CUTOFF':20,
                'CONFIDENCE':1,
                'MODELFILE':"models/"+modelfile,
                'FILELIST':"%s" % self.fl_name
                }
        self.set_config()
        self.data = {}
    def set_config(self,**kwargs):
        if kwargs:
            for k,v in kwargs.iteritems():
                self.params.update({k:v})
        f = open(self.configfile,'w')
        for k,v in self.params.iteritems():
            f.write('%s %s\n' % (k,v))
        f.close()
    def ingest(self,filename=None,lang='unk',
               rm=False,rand=False,sample_size=1):
        if filename:
            data = [tuple(r) for r in read_txt(filename,lang)]
        if rand: 
            sample_size = sample_size or len(data)
            if sample_size>len(data):
                sample_size = len(data)
            data = [data[i] for i in 
                    sample(range(len(data)),sample_size)]
        if rm: self.cleanup()
        for fid, text in [(r[0],r[1]) for r in data]:
            self.fileprep(text,fid,lang)
        self.data.update(((d[0],lang if d[2]=='unk' else d[2]) 
                for d in data))
    def fileprep(self,text,fid,lang):
        datadir = self.tmpdir+"/"+lang
        if not os.path.exists(datadir):
            os.makedirs(datadir)
        script = self.script or SCRIPTS[lang]
        f = open("%s/%0s.txt" % (datadir,fid),"w")
        f.write(preprocess(text,script).encode('utf8'))
        f.close()
    def filelist(self,data):
        fl = open(self.fl_name,"w")
        for fid,label in data.items():
            line = ''.join(('%(d)s/%(l)s/%(f)s.txt %(l)s\n' % 
                    {'d':self.tmpdir,'f':fid,'l':label}))
            fl.write(line)
        fl.close()
    def train(self,data=None):
        self.filelist(data or self.data)
        os.system(("%sbin/PLAINclassify train %s > /dev/null") % 
                (self.root,self.configfile))
    def classify(self,data=None,text=None,lang='unk',threshold=0.3):
        if data:
            self.filelist(data)
            testdata = [(fid,label) for fid,label in data.items()]
        elif text:
            fid = hash(time())
            self.fileprep(text,fid,lang)
            self.filelist(data={fid:lang})
            testdata = [(fid,'unk')]
        else:
            print "Nothing to identify."
            return
        os.system("%sbin/PLAINclassify test %s > %s" %
                (self.root, self.configfile, self.rf_name))
        f = open(self.rf_name)
        results = [(re.sub(" .*$|\n","",r),
                    float(re.sub("^.*? |\n","",r))) 
                    for r in f if not re.match(r'////\n|\x00',r)]
        f.close()
        os.system("rm %s" % self.rf_name)
        labels = [((testdata[d][0], results[d][0],
                   results[d][1]) if results[d][1] > threshold 
                        else (testdata[d][0],'unk',0.0)) 
                   for d in range(len(results))]
        return labels
    def cleanup(self):
        try:
            os.system('rm %s' % self.fl_name)
            os.system('rm %s' % self.configfile)
            os.system('rm -rf %s' % self.tmpdir)
        except Exception, e:
            print e
            pass


def max_script(text):
    run_types = defaultdict(int)
    totalCount = 0
    for c in text:
        if c.isalpha():
            block = unicodeBlock(c)
            run_types[block] += 1
            totalCount += 1
    if "Hangul Syllables" in run_types.keys() or \
            "Hangul Jamo" in run_types.keys() or \
            "Hangul Compatibility Jamo" in run_types.keys() or \
            "Hangul" in run_types.keys():
        return "Hangul"
    elif "Katakana" in run_types.keys():
        return "Katakana"
    else:
        return max(run_types.iteritems(), key=itemgetter(1))[0]


def identify(text,model='all'):
    script = max_script(text)
    if len(text) < 3:
        return 'unk'
    # Try languages with unique scripts
    if script in SINGLETONS.keys():
        return (SINGLETONS[script],1.0)
    # Otherwise, model language within script
    cl = None
    if script in ["Arabic", 
                  "Arabic Presentation Forms-A",
                  "Arabic Presentation Forms-B"]:
        cl = PLAINClassifier(script="arabic",modelfile=model)
    elif script == "Cyrillic":
        cl = PLAINClassifier(script="cyrillic",modelfile=model)
    elif script == "Devanagari":
        cl = PLAINClassifier(script="devanagari",modelfile=model)
    elif script in ["Basic Latin", "Basic Latin Extended"]:
        cl = PLAINClassifier(script="latin",modelfile=model)
    if cl:
        lang, p = cl.classify(text=text)[0][1:3]
        cl.cleanup()
        return (lang, p)
    else:
        return ('unk',0.0)

def speed_test():
    f = open("data/original/speed_test.txt","r")
    test_data = [d.decode('utf8') for d in f.readlines()]
    f.close()
    times = []
    res = []
    for dat in test_data:
        x = time()
        res += [identify(dat)]
        times += [time()-x]
    print mean(times)


if __name__ == '__main__':

    if not os.isatty(0):
        con = sys.stdin
    elif len(sys.argv)==2 and (sys.argv[1] not in ('-h','--help')):
        con = file(sys.argv[1],'r')
    else:
        sys.exit("Usage: <text> | ./lid.py")
    
    for line in con:
        lang, p = identify(text=line.strip().decode('utf8'), model='major')
        print "%s (p=%0.2f)" % (NAME_MAP[lang],p)

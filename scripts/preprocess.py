#! /usr/bin/python
# -*- coding: utf-8 -*-

# Perform various normalizations on ar/fa/da/ps/ur text.

import re
import sys
from unicodedata import category 
from itertools import chain



PR = {
    'arabic':
        [(re.compile(p[0],re.U),p[1]) for p in [ 
        (u"[\u200e-\u200f]|[\u202a-\u202e]",u""), # rllr -> 0
        (u"[\u202b-\u202f]|[\u205f-\u2064]",u""), # invis -> 0
        (u"[^\u0600-\u06ff ]",u" "), # non-arabic -> 0
        (u"[\u0656-\u065e\u06d6-\u06ed]",u""), # diac -> 0
        (u"[\u063b\u063c\u06a9]",u"\u0643"), # keheh -> kaf
        (u"\u06ab",u"\u06af"), # ps:gaf -> fa:gaf
        (u"[\u0622\u0623\u0625\u0671]",u"\u0627"), # alef+diac -> alef
        (u"[\u0624\u06cf]",u"\u0648"), # waw+diac -> waw
        (u"[\u0629\u06c1\u06be]",u"\u0647"), # teh marbuta, ur:heh -> ar heh
        (u"[\u0626\u0649\u06cc]",u"\u064a"), # fa:yeh, alef maksura -> ar yeh
        (u"\u066f",u"\u0642"), # dotless qaf -> qaf
        (u"\u0640+",u""), # tatweel -> 0
        (u"[\u0660-\u0669\u06f0-\u06f9]",u""), # num -> 0
        ]],
    'cyrillic':
        [(re.compile(p[0],re.U),p[1]) for p in [ 
        (u"[^\u0400-\u052f ]",u" "), # non-cyrillic -> 0
        ]],
    'devanagari':
        [(re.compile(p[0],re.U),p[1]) for p in [ 
        (u"[^\u0900-\u097f\ua8e0-\ua8ff ]",u" "), # non-devanagari -> 0
        ]],
    'latin':
        [(re.compile(p[0],re.U),p[1]) for p in [ 
        (u"[^\u0000-\u02af ]",u" "), # non-latin -> 0
        ]],
    'general':
        [(re.compile(p[0],re.U),p[1]) for p in [ 
        (u"[0-9]",u""), # num -> 0
        (u"(.)(\\1){2,}",u"\\1"), # CCC+ -> C
        (u"[\W]",u" "), # non-word -> space
        (u"(^ +| +$|\s(?=\s))",u""), # extra whitespace -> 0
        ]],
    }


def strip_NNP(text):
    lwr = lambda p: p.group(1) if p.group(1)[0].islower() else ''
    return re.sub('(?<=[^.!,;:] )(.+?)(?:(?=$)|(?=[ .!,;:]))',lwr,text,re.U)


def char_ngrams(text,n=3):
    spl = n==1 and '%s' or '_%s_'
    grams = [[s[i:i+n] for i in range(len(s)-n+1)] 
            for s in [spl % w for w in text.split(' ') if w != '']]
    return ' '.join([g for g in chain.from_iterable(grams)])


def preprocess(text,script="unk",nmin=3,nmax=3):
    if script in ['latin','cyrillic']:
        #text = strip_NNP(text)
        text = text.lower()
    if script is not "unk":
        for ptn, rep in PR[script]:
            text = ptn.sub(rep,text)
    for ptn, rep in PR['general']:
        text = ptn.sub(rep,text)
    grams = [] #[text]
    for n in range(nmin,nmax+1):
        grams.append(char_ngrams(text,n))
    return ' '.join(grams)


if __name__ == '__main__':
    pass
#   for line in sys.stdin:
#       sys.stdout.write((preproc(unicode(line, 'utf-8'), nmin=3, nmax=3)+' ').encode('utf-8'))
#       #sys.stdout.write(normalize(unicode(line, 'utf-8')).encode('utf-8'))


#!/usr/bin/python
# encoding: utf-8

"""
@author: graham
"""

from sgrab import *

if __name__ == '__main__': 

    langs = [
            # 100+
            'kbd', 'ab', 'av', 'bm', 'bi', 'bxr', 'ch', 
            'ny', 'za', 'dsb', 'ee', 'ff',
            'gag', 'ki', 'got', 'ha', 'ig', 'iu', 'ik', 
            'ks', 'kg', 'lo', 'lbe',
            'ltg', 'lg', 'cdo', 'mwl', 'mdf', 
            'mo', 'fj', 'na', 'cr', 'pih',
            'om', 'as', 'pnt', 'kaa', 'dz', 'rmy', 
            'rn', 'sm', 'sg', 'st', 'tn',
            'sn', 'sd', 'cu', 
            'ss', 'srn', 'ty', 'kab', 'tet', 'ti', 'chr',
            'tum', 'ts', 've', 'xh', 'zea', 'zu',
            # 1k+
            'ace', 'als', 'frp', 'arc',
            'gn', 'ay', 'zh-min-nan', 'bjn', 'map-bms', 'ba', 'bh', 'bcl',
            'bar', 'bo', 'cbk-zam', 'co', 'pdc', 'dv', 
            'nv', 'ang', 'eml', 'myv', 'ext', 'hif', 'fo', 
            'frr', 'fur', 'gv', 'gd', 'gan', 'glk', 'hak', 'xal', 
            'haw', 'hsb', 'ilo', 'ia', 'ie', 'os', 'kl', 'pam',
            'csb', 'kw', 'km', 
            'rw', 'kv', 'ky', 'mrj', 'lad', 'lij', 'li',
            'ln', 'jbo', 'mg', 'mt', 'zh-classical', 'mi', 'arz', 'mzn', 'mn',
            'my','nah', 'nds-nl', 'nrm', 'nov', 'ce', 'mhr', 'or', 'uz', 'pi',
            'pag', 'pa', 'pap', 
            'ps', 'koi', 'pfl', 'pcd', 'krc', 'crh', 'rm',
            'rue', 'sa', 'se', 'sc', 'sah', 'sco', 'stq', 'si', 'szl', 'so',
            'ckb', 'roa-tara', 'tt', 'tg', 'tpi', 'to', 'tk', 'udm', 'bug',
            'ug', 'vec', 'fiu-vro', 'vls', 'wo', 'wuu', 'yi', 'diq', 
            # 10k+
            'an', 'roa-rup', 'ast', 'ht', 'az',
            'bn', 'be', 'be-x-old', 'bpy', 'bs', 
            'br', 'cv', 'cy', 'et', 'el',
            'fy', 'ga', 'gl', 'gu', 'hy', 
            'hi', 'hr', 'io', 'is', 'jv', 'kn',
            'ka', 'ku', 'la', 'lv', 'lb', 'lmo', 
            'mk', 'ml', 'mr', 'new', 'ne',
            'nn', 'nap', 'oc', 'pms', 'nds', 'kk', 'ksh', 
            'qu', 'pnb', 'sq',
            'scn', 'simple', 'ceb', 'sh', 'su', 'sw', 'tl', 'ta', 'te', 'th',
            'ur', 'wa', 'yo', 'zh-yue', 'bat-smg', 
            # 100k+
            'eo', 'eu', 'ko', 'lt', 'hu', 'ms', 'pl', 'uk', 'vi',
            'vo', 'war', 
            ]

    for lang in langs:
        cr = SummaryCrawler(
            limit=2000,
            timeout=30,
            minwd=5,
            ppc=0.05, 
            wpt=0,
            wps=50,
            outfile="",
            verbose=True,
            lid_model="major",
            lid_threshold=0.6)
        cr.outfile = "%s_sentences.txt" % lang
        cr.set_url("http://%s.wikipedia.org/" % lang)
        cr.get_summaries()


SGrab
=====

Author: Graham Horwood  
Created: 2011-07-01  
Modified: 2013-06-20  

This collection of scripts allows a user to scrape an arbitrary website for sentences of text. The 'sgrab.py' script will spider through the website's pages, starting from a designated URL (or set of URLS in a text file) and ending when a designated number of page requests has been reached. On each page, the script removes "cruft" elements (menues, adds, banners, etc.), performs some light text processing and sentence tokenization on remaining blocks of body text, and returns well-formed sentences of text.


Operation
---------

The script is run from the base 'sgrab' directory. For example, the following command would spider the Aljazeera website and write out collected sentences to 'aljaz.txt'.

    ./scripts/sgrab.py -v http://aljazeera.net aljaz.txt

Setting the '-v' switch will write system messages and captured text to STDOUT. Captured document text is color-coded blue, with document text falling outside filtration parameters (see below) color-coded red.


Configuration
-------------

The script is designed to work in as format-agnostic a manner as possible, allowing the user to extract content from any arbitrary site without significant configuration for site or page structure. A few simple text analytics are provided for filtration purposes, however, as shown below with script defaults. 

* Minimum document length in words [--minwd=50]: This might be set higher if a given site uses document stubs or other filler content.

* Maximum document length in words [--maxwd=10000]: Current setting intended to improve performance. Parameter could be raised for extraction of content from sites which post, for example, entire novels on one page.

* Maximum punctuation marks per character [--ppc=0.05]: Useful for filtering out pages of dates, bibliographic references, and other non-sentential text.

* Minimum words per html tag [--wpt=2]: Useful for catching pages which 'decruft.py' mis-parses.

* Maximum words per sentence [--wps=50]: Useful for filtering out ill-formed sentences.

In verbose mode (-v), the script will write out word count, ppc, wpt, and wps calculations for non-empty page, allowing the user to make appropriate adjustments. For example: 

    Spidering: http://aljazeera.net/NR/exeres/84F23948(...)
      wc:595, ppc:0.02, wpt:3.31, wps:24.79
    تظاهر آلاف السوريين أمس الخميس في حلب ثاني (...) 

Additionally, a language identifier may be used strip out sentences of confounding languages (typically English). In addition to a model file, a minimum classification threshold may be set, and should be adjusted for the linguistic similarity of the desired content w.r.t. the confounding content.

* LID model filename [--lidmodel=major]
* Minimum p(lang|sentence) threshold [--lidthreshold=0.5] 


Known bugs
----------

The script tokenizes sentences at commonly used terminal punctuation:

    [.?!\u0964\u0965\u10fb\u203b-\u203d\u2047-\u2049\u1367\u1368\u061f]

Elipses are ignored, but no more sophisticated tokenization is done at this time. 

The browser object it uses (mechanize) cannot interpret javascript, meaning that no text will be extracted from sites which render all their textual content dynamically.


Updates
-------

20130620: The [-R] parameter will disable observance of robots.txt.

20130620: The data directory contains a newline delimited file 'blacklist.txt' which itemizes file extensions which are not downloaded by the spider. 



